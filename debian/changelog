cothreads (0.10-5) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Tue, 17 Sep 2019 09:47:14 +0200

cothreads (0.10-4) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Switch source package format to 3.0 (quilt)
  * Bump debhelper compat to 10
  * Update Vcs-*

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders (Closes: #869317)

 -- Stéphane Glondu <glondu@debian.org>  Thu, 27 Jul 2017 06:24:50 +0200

cothreads (0.10-3) unstable; urgency=low

  [ Sylvain Le Gall ]
  * Use dh_ocaml (and related tools) to build the package and compute
    the dependencies.
  * Move the package to section ocaml.

  [ Mehdi Dogguy ]
  * Cosmetic changes in debian/control
  * Update email address
  * Bump standards to 3.8.3, no changes needed
  * Build-depend on OCaml 3.11.1-3~
  * Bump dh-ocaml version to 0.9.1
  * Switch order of inclusion for debhelper.mk and dpatch.mk to
    workaround a bug of cdbs.
  * Add a nodefined-map for dh-ocaml to not export some modules
    (Closes: #549784).

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 31 Oct 2009 13:36:33 +0100

cothreads (0.10-2) unstable; urgency=low

  * Add build-depends on dh-ocaml
  * Upgrade Standards-Version to 3.8.0 (README.source)
  * Switch packaging to git
  * Remove verbatim text in debian/control
  * Update homepage field
  * Use rules/ocaml.mk, add versionned dependency on dh-ocaml
  * Update debian/copyright file to use
    http://wiki.debian.org/Proposals/CopyrightFormat

 -- Sylvain Le Gall <gildor@debian.org>  Fri, 06 Mar 2009 22:56:54 +0100

cothreads (0.10-1) unstable; urgency=low

  [ Erik de Castro Lopo ]
  * Initial upload (Closes: #444082)

  [ Sylvain Le Gall ]
  * Use ocaml 3.10.0-9 for generating .ocamldoc-apiref automatically

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Mehdi Dogguy ]
  * enhance META
  * add myself as an Uploader
  * set Maintainer to d-o-m, and Erik as uploader
  * bump standards-version
  * remove line '@OCamlStdlibDir@/cothreads' from libcothreads-ocaml-dev.dirs.in
    (thanks lintian)

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 10 May 2008 00:47:16 +0200
